use clap::{
    Command,
    Arg,
    error::Error
};
fn main() -> Result<(),Error>{
    let matched_arguments = Command::new("My Program")
    .author("GueLaKais, koroyeldiores@gmail.com")
    .version("1.0.2")
    .about("Explains in brief what the program does")
    .arg(Arg::new("build-type")
        .long("build-type").required(false).default_value("ament_cargo")
        .help(
r#"
Available Build types for ros2 are:
- ament_python: Python packages
- ament_cmake: c++ Packages
- ament_cargo: rust packages
"#
            ))
    .arg(Arg::new("Package-Format")
        .long("Package_format").alias("Package-Format").required(false).default_value("3")
        .help(r#"
Available Package Formats are:
-3:
              "#)
        )
    .arg(Arg::new("Package Name").long("Package_Name").short('n')
    .aliases(["pN"]).required(true).help(r#"
Name Of the Package
    "#))
    .arg(Arg::new("description")
        .long("description").required(false)
        .default_value("TODO: Description")
        .help(r#"
The Description of your Package. should be a String.
              "#))
    .arg(Arg::new("Destination Directory").long("destination-directory")
        .required(false).default_value("./")
        .help("The path, where your Package has to be built in"))
    .arg(Arg::new("Library Name").long("library-name").required(false)
        .help("The place to store all your additional source code to get more order")
        )
    .arg(Arg::new("licence").long("licence").required(false).default_value("Apache-2.0").help(
r#"
The Licence of your Project. Standard Licences are:
Apache-2
GPL
"#))
    .arg(Arg::new("Node name").long("node-name").required(false).help(
r#"
Name of one node of your Project. under Rust, it's the binary target.
"#))
    .arg(Arg::new("Maintainer Name").long("maintainer-name").required(false)
         .default_value("maintainer").help("Name of the Package Maintainer"))
    .arg(Arg::new("Maintainer Email").long("maintainer-email").required(false)
         .default_value("maintainer").help("Email address of the Package Maintainer"))
    .after_help("Longer explanation to appear after the options when \
                 displaying the help information from --help or -h")
    .get_matches();
    println!("{}", matched_arguments.get_one::<String>("Package Name").unwrap());
    Ok(())
}
