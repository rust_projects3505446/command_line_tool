use anyhow::Error;
use std::process::Command;
use tokio::runtime::Runtime;

enum CargoArgs {
    Default,
    New,
    Build,
    Run,
}

struct CliArgs {
    cargo_args: CargoArgs,
    arg: String,
}

fn cargo_input(argument: &str) -> CargoArgs {
    match argument {
        "new"|"New"|"NEW" => CargoArgs::New,
        "build"|"Build"|"BUILD" => CargoArgs::Build,
        "run"|"Run"|"RUN"=> CargoArgs::Run,
        _ => CargoArgs::Default,
    }
}

fn input_handling() -> CliArgs {
    let args: Vec<String> = std::env::args().collect();
    CliArgs {
        cargo_args: cargo_input(args[1].as_str()),
        arg: args[2].clone(),
    }
}

fn create_new_package(name: &str) -> Result<(),Error> {
    println!("{:?}",  Command::new("cargo")
    .arg("new")
    .arg(name)
    .output()
    .expect("Failed to execute command"));
    Ok(())
}
fn build_package(name: &str) -> Result<(),Error> {
    println!("{:?}",  Command::new("cargo")
    .arg("build").arg(name).output()
    .expect("Failed to execute command"));
    Ok(())
}

fn output_handling(args: CliArgs) -> Result<(),Error> {
    let rt = Runtime::new().unwrap();
    rt.block_on(async {
        match args.cargo_args {
            CargoArgs::Default => println!("default"),
            CargoArgs::New => create_new_package(&args.arg).unwrap(),
            CargoArgs::Run => println!("running:{}", args.arg),
            CargoArgs::Build => build_package(&args.arg).unwrap(),
        }
    });
    Ok(())
}

fn main() -> Result<(),Error> {
    output_handling(input_handling())
}
