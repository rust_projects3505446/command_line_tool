struct Cli {
    pattern: String,
    path: std::path::PathBuf,
}
fn main() {
    let args = Cli {
        pattern: std::env::args().nth(1).expect("no pattern given"),
        path: std::path::PathBuf::from(std::env::args().nth(2).expect("no path given")),
    };

    println!("pattern: {:?}, path: {:?}", args.pattern, args.path);
}
