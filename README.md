# Command_line_tool
## Introduction
The main goal of this package is to provide some simple examples of 
command line tools written in Rust. For me, it's just to gain experience 
in writing Rust programs. For you, it's just to provide some examples 
that you can use as a base for your own projects.
## Getting started
To use one of these examples, just type it in:
```
cargo run --example name_of_example -- argument_one argument_two
```
For example, the following shell input:
```
cargo run --example cli_clap --release -- udo bob
```
produces as Output:
```
pattern: "udo", path: "bob"
```
